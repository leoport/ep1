#ifndef PESSOA_HPP
#define PESSOA_HPP

#include  <iostream>

using namespace std;

class Pessoa {

public:
	void set_nome(string nome);
	virtual	string get_nome();
	virtual void registro_de_pessoa();

private:
	string nome;

};
#endif
//
