#ifndef ELEITORES_HPP
#define ELEITORES_HPP
#include "pessoas.hpp"

class Eleitor: public Pessoa { // usado de herança entre classes 

private:
	
	int pos_Deputado_Federal=-1;
	int pos_Deputado_Distrital=-1;
	int pos_Senador_1=-1; // guardar a posição no vetor de objetos
	int pos_Senador_2=-1;
	int pos_Governador=-1;
	int pos_Presidente=-1;
	int voto_branco=0;

public:
	
	//Eleitor();
	//~Eleitor();
	void set_pos_Deputado_Federal(int pos_Deputado_Federal); 
	int get_pos_Deputado_Federal();

	void set_pos_Deputado_Distrital(int pos_Deputado_Distrital); 
	int get_pos_Deputado_Distrital();

	void set_pos_Senador_1(int pos_Senador_1); 
	int get_pos_Senador_1();

	void set_pos_Senador_2(int pos_Senador_2); 
	int get_pos_Senador_2();

	void set_pos_Governador(int pos_Governador); 
	int get_pos_Governador();

	void set_pos_Presidente(int pos_Presidente); 
	int get_pos_Presidente();

	//void set_vot_branco();

	void registro_de_pessoa(string nome, int pos_Deputado_Federal,  int pos_Deputado_Distrital, // a váriavel nome é herdada  
	int pos_Senador_1, int pos_Senador_2, int pos_Governador, int pos_Presidente);              // da classe pessoa

};

#endif
