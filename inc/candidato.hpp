#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP

#include "pessoas.hpp"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Candidato : public Pessoa {
private:
	int numero_de_votos=0;
	string cargo;
	string numero_do_candidato;
	string nome_na_urna;
	string cpf_do_candidato;
	string email_do_candidato;
	string DS_situacao_candidatura;
	string DS_detalhe_situacao_candidatura;
	string agremiacao;
	string sigla_do_partido;
	string nome_do_partido;
	string SQ_coligacao;
	string composicao_coligacao;
	string ocupacao;

public:
	Candidato();
	~Candidato();
	int get_numero_de_votos();
	void set_numero_de_votos();

	string get_cargo();
	void set_cargo(string cargo);

	string get_numero_do_candidato();
	void set_numero_do_canditado(string numero_do_candidato);

	string get_nome_na_urna();
	void set_nome_na_urna(string nome_na_urna);

	string get_cpf_do_candidato();
	void set_cpf_do_candidato(string cpf_do_candidato);

	string get_email_do_candidato();
	void set_email_do_candidato(string email_do_candidato);

	void registro_de_pessoa(const char *path, int candidato); //aplicação de sobrecarga de operadores (polimorfismo)

	void imprimir_dados(Candidato candidatos);


};



#endif
