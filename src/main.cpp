#include <bits/stdc++.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "candidato.hpp"
#include "eleitores.hpp"

void imprimir_opcoes_da_urna(char codigo[], int cont);

using namespace std;

int main() {
    cout << "EP1 2018_2" << endl;

    Candidato candidatosBR[26], candidatosDF[1237];
	for (int i=0; i<26; i++) {
		candidatosBR[i].registro_de_pessoa("../data/consulta_cand_2018_BR.csv", i);
	}
	for (int i=0; i<1237; i++) {
		candidatosDF[i].registro_de_pessoa("../data/consulta_cand_2018_DF.csv", i);
	}


	int k=-1, j=0; //contadores
	int quant_eleitores=0;
	int confirmar=0;
	string nome_aux;
	int tamanho_do_codigo=0;
	string nome_do_eleitor;
	char tecla;


	cout << "Mesário, por favor, informe a quantidade de eleitores." << endl;
	cin >> quant_eleitores;
	
	vector <Eleitor> eleitor;
	Eleitor eleitores_obj;

	
	for (j=0; j< quant_eleitores; j++) { // cadastro e votos dos eleitores
		getchar();
		system("clear");		
		cout << "Caro elitor, por favor, insira seu nome."<< endl;
		getline(cin, nome_aux);       // ler o nome do eleitor e "armazená-lo numa classe" eleitor que herda de pessoa
		eleitor.push_back(eleitores_obj);
		eleitor[j].set_nome(nome_aux);
		int votos_do_eleitor = 0;

		while (votos_do_eleitor<6){  // votação
			int pos_presidente=0, pos_vicepresidente=0, pos_governador=0, pos_vicegovernador=0, pos_senador=0;
				int pos_prisuplente=0;
				int pos_segsuplente =0;
				int k;				
			if (votos_do_eleitor == 0) { // Deputado Federal primeiro cargo
				tamanho_do_codigo = 4;
			}
			else if (votos_do_eleitor == 1) { // Deputado Distrital
				tamanho_do_codigo = 5;
			}
			else if (votos_do_eleitor == 2 || votos_do_eleitor == 3 ) { // Senadores
				tamanho_do_codigo = 3;
			}
			else if (votos_do_eleitor == 4) { // Governador
				tamanho_do_codigo = 2;
			}
			else if (votos_do_eleitor == 5) { // Presidente;
				tamanho_do_codigo = 2;
			}

			char codigo[6]="";
			int i=0;

			imprimir_opcoes_da_urna(codigo, votos_do_eleitor);
			
			while (true) {	
				
				cin >> codigo[i];
				if (codigo[i]=='b') {
					codigo[i+1] ='\0';
					break;
				}
				if (i == tamanho_do_codigo -1 && codigo[i]!='l') {
					codigo[i+1] = getchar();
					if (codigo[i+1]!='l') {
						codigo[i+1]='\0';
						imprimir_opcoes_da_urna(codigo, votos_do_eleitor);
					break;

					} else {
						i++;
					}
				}
				if (i== tamanho_do_codigo && codigo[i]!='l'){	
					codigo[i] = '\0';
					imprimir_opcoes_da_urna(codigo, votos_do_eleitor);
					break;
				}
				if (codigo[i]=='l') {
					codigo[i-1]='\0';
					i--;
				} else {
					i++;
				}
				imprimir_opcoes_da_urna(codigo, votos_do_eleitor);
			} // aqui também deve entrar as opção de votar em branco e de apagar caractére;
		//	cout<< "aqui " << votos_do_eleitor << endl;
		//	getchar();
		//	getchar();
			if (votos_do_eleitor == 5) { // presidente
			
				for(k=0; k< 26; k++) {
					if(codigo==candidatosBR[k].get_numero_do_candidato()){
						if (candidatosBR[k].get_cargo()=="PRESIDENTE") {
							pos_presidente = k;
						}
						else if(candidatosBR[k].get_cargo()=="VICE-PRESIDENTE") {
							pos_vicepresidente = k;	
						}
					}
				}

				system("clear");

				candidatosBR[pos_presidente].imprimir_dados(candidatosBR[pos_presidente]);
				cout << "------------------------------------------------------";
				candidatosBR[pos_vicepresidente].imprimir_dados(candidatosBR[pos_vicepresidente]);
			}	
			else if (votos_do_eleitor == 4) {	
				for(k=0; k< 1237; k++) {
					if(codigo==candidatosDF[k].get_numero_do_candidato()){
						if(candidatosDF[k].get_cargo()=="GOVERNADOR") {
							pos_governador = k;	
						}
						else if(candidatosDF[k].get_cargo()=="VICE-GOVERNADOR") {
							pos_vicegovernador = k;	
						}
					}
				}
				system("clear");
				candidatosDF[pos_governador].imprimir_dados(candidatosDF[pos_governador]);
				cout << "------------------------------------------------------";
				candidatosDF[pos_vicegovernador].imprimir_dados(candidatosDF[pos_vicegovernador]);

			}
			else if (votos_do_eleitor ==2 || votos_do_eleitor ==3) {
				for(k=0; k< 1237; k++) {
					if(codigo==candidatosDF[k].get_numero_do_candidato()){
						if(candidatosDF[k].get_cargo()=="SENADOR") {
							pos_senador = k;	
						}
						else if(candidatosDF[k].get_cargo()!="SENADOR" && pos_prisuplente==0) { //BUG COM O NOME SUPLENTE 
							pos_prisuplente = k;	
						}
						else if(candidatosDF[k].get_cargo()!="SENADOR" && pos_prisuplente!=0) {
							pos_segsuplente =k;
						}
					}
				}
				system("clear");
				candidatosDF[pos_senador].imprimir_dados(candidatosDF[pos_senador]);
				cout << "------------------------------------------------------";
				candidatosDF[pos_prisuplente].imprimir_dados(candidatosDF[pos_prisuplente]);
				cout << "------------------------------------------------------";
				candidatosDF[pos_segsuplente].imprimir_dados(candidatosDF[pos_segsuplente]);
			}
			else if (votos_do_eleitor == 0 || votos_do_eleitor == 1){
				
				for(k=0; k< 1237; k++) {
					if(codigo==candidatosDF[k].get_numero_do_candidato()){
						break;
					}
				}
				system("clear");

				candidatosDF[k].imprimir_dados(candidatosDF[k]);
				
			}		
			
			cout << "-----------------------------------------------------"<< endl<< endl;
	
			while (true) {
				
				cout << "Pressione Enter após qualquer escolha."<< endl;
				cout << endl << "Dite a tecla \"c\" para confirmar voto." << endl;
				cout << "Dite a tecla \"v\" para cancelar o voto e retornar."<< endl<< endl;
				cin >> tecla;
				if (tecla=='c') { // confirmar
				//computar o voto para o candidato;
					if (votos_do_eleitor==0) {
						eleitor[j].set_pos_Deputado_Federal(k);
						candidatosDF[k].set_numero_de_votos();
					}
					else if(votos_do_eleitor==1) {
						eleitor[j].set_pos_Deputado_Distrital(k);
						candidatosDF[k].set_numero_de_votos();
					}
					else if(votos_do_eleitor==2) {
						eleitor[j].set_pos_Senador_1(pos_senador);
						candidatosDF[pos_senador].set_numero_de_votos();
					}
					else if(votos_do_eleitor==3) {
						eleitor[j].set_pos_Senador_2(pos_senador);
						candidatosDF[pos_senador].set_numero_de_votos();
					}
					else if(votos_do_eleitor==4) {
						eleitor[j].set_pos_Governador(pos_governador);
						candidatosDF[pos_governador].set_numero_de_votos();
					}
					else if (votos_do_eleitor==5) {
							eleitor[j].set_pos_Presidente(pos_presidente);
							candidatosBR[pos_presidente].set_numero_de_votos(); 
					}
					votos_do_eleitor ++;
					break;
				}
				else if (tecla == 'v') {
					break;
				} 
				system("clear");
				cout << "Dígito invávilo, por favor, pressione Enter e tente novamente."<< endl;
				getchar();
				getchar();
				system("clear");
			}
			
				
		}
		
		system("clear");
		cout << "Nome do eleitor: " << eleitor[j].get_nome()<<endl<<endl;
		cout << "Votos: "<<endl;
		cout <<"Deputado Federal: "<< candidatosDF[eleitor[j].get_pos_Deputado_Federal()].get_nome()<<endl;
		cout <<"Deputado  Distrital: "<<candidatosDF[eleitor[j].get_pos_Deputado_Distrital()].get_nome()<<endl;
		cout <<"Senador: " <<candidatosDF[eleitor[j].get_pos_Senador_1()].get_nome()<<endl;
		cout <<"Senador: " << candidatosDF[eleitor[j].get_pos_Senador_2()].get_nome()<<endl;
		cout <<"Goverandor: " << candidatosDF[eleitor[j].get_pos_Governador()].get_nome()<<endl;
		cout <<"Presidente: " << candidatosBR[eleitor[j].get_pos_Presidente()].get_nome()<<endl;
	}

	int maior=0, posicao=0;

	for(int i=0; i<26; i++) {
		if (candidatosBR[i].get_numero_de_votos()>maior) {
				maior = candidatosBR[i].get_numero_de_votos();
				posicao = i;
		}
	}


    return 0;
}


void imprimir_opcoes_da_urna(char codigo[], int cont){
	system("clear");//	cout << "aqui" << endl;
	cout << endl;
	if (cont == 0) {
		cout<< "Digite o código do seu Deputado Federal." << endl;
	}
	else if (cont == 1) {
		cout<< "Digite o código do seu Deputado Distrital." << endl;
	} 
	else if (cont == 2) {
		cout<< "Digite o código do seu primeiro Senador." << endl;
	}
	else if (cont == 3) {
		cout<< "Digite o código do seu segundo Senador." << endl;
	}
	else if (cont == 4) {
		cout<< "Digite o código do seu Governador." << endl;
	}
	else if (cont == 5) {
		cout<< "Digite o código do seu Presidente." << endl;
	}

//	cout << "Digite o codigo do deputado Distrital: " << endl;
	cout << "----------------------------------------" << endl;
	cout << "Digite a tecla \"l\" para limpar digito."<< endl;
	cout << "----------------------------------------" << endl;
	cout << "Digite a tecla \"b\" para votar nulo."<< endl<<endl;
	
	printf("%s", codigo);
}

int busca_do_candidato (Candidato candidato, string codigo, int tamanho) {
//	candidato
	return 1;
}

